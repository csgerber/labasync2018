package edu.uchicago.gerber.labasync;

import android.graphics.Color;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.Date;
import java.util.Random;

public class MainActivity extends AppCompatActivity {



    private Button button;
    private LinearLayout root;
    private ProgressBar progressBar;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        button = findViewById(R.id.button);
        root = findViewById(R.id.root);
        progressBar = findViewById(R.id.progressBar);

        root.setBackgroundColor(Color.parseColor("#33292bff"));

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                 new SampleTask().execute(System.currentTimeMillis());

            }
        });




    }//end onCreate()



    private class SampleTask extends AsyncTask<Long, Integer, String>{




        //UI Thread
        @Override
        protected void onPreExecute() {

            root.setBackgroundColor(Color.parseColor("#339922ff"));
            button.setText("working");
            super.onPreExecute();
        }

        //Background thread
        @Override
        protected String doInBackground(Long... longs) {

            for (int nC = 0; nC <= 100; nC++) {
                try {
                        Thread.sleep(50);

                    publishProgress(nC);

                } catch (InterruptedException e) {
                    e.printStackTrace();
                }




            }
            return System.currentTimeMillis() - longs[0] + " was your time.";

        }

        //UI Thread
        @Override
        protected void onProgressUpdate(Integer... values) {

            progressBar.setProgress(values[0]);

            super.onProgressUpdate(values);
        }

        //UI Thread
        @Override
        protected void onPostExecute(String s) {
            root.setBackgroundColor(Color.parseColor("#33292bff"));
            button.setText("click-me");
            progressBar.setProgress(0);

            Toast.makeText(MainActivity.this, s, Toast.LENGTH_SHORT).show();

            super.onPostExecute(s);
        }



    }


}
